import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.math.BigInteger;
import java.util.Arrays;


public class Main {

    public static void main(String[] args) throws IOException {
        FileReader input = new FileReader("file.txt");
        BufferedReader bufRead = new BufferedReader(input);
        String myLine = null;
        String cvsSplitBy = ",";
        int temp;
        System.out.println("------------Ex. 2-------------");

        while ((myLine = bufRead.readLine()) != null) {
            String[] array1 = myLine.split(cvsSplitBy);
            System.out.println("File consists: " + myLine);
            int numArr[] = new int[array1.length];
            for (int i = 0; i < array1.length; i++) {
                numArr[i] = Integer.parseInt(array1[i]);

            }
            Arrays.sort(numArr);
            System.out.println("File sorted by numbers: ");
            System.out.println(Arrays.toString(numArr));

            //reverse sorting
            for( int i = 0; i < numArr.length/2; ++i )
            {
                temp = numArr[i];
                numArr[i] = numArr[numArr.length - i - 1];
                numArr[numArr.length - i - 1] = temp;
            }
            System.out.println(Arrays.toString(numArr));





        }

        System.out.println("------------Ex. 3-------------");
        System.out.println("Factorial 20 = ");
        System.out.println(factorial(20));
    }

    public static BigInteger factorial(int n)
    {
        BigInteger res = BigInteger.valueOf(1);
        for (int i = 2; i <= n; i++){
            res = res.multiply(BigInteger.valueOf(i));
        }
        return res;
    }
}

